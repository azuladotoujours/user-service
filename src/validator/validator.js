exports.userSignUpValidator = (req, res, next) => {
  req
    .check('identity_card', 'Identity card must have between 5 and 10 digits')
    .matches(/[0-9]{5,10}/)
    .notEmpty();
  //NAMES ARE NOT NULL
  req
    .check('names', 'Names are required')
    .notEmpty()
    .matches(/[a-zA-Z]{3,40}/);
  //LAST NAMES ARE NOT NULL
  req
    .check('last_names', 'Last names are required')
    .notEmpty()
    .matches(/[a-zA-Z]{3,40}/);
  //MOBILE MUST HAVE TEN DIGITS AND NOT BE NULL
  req
    .check('mobile', 'Mobile must have ten digits')
    .matches(/[0-9]{10}/)
    .notEmpty();
  //EMAIL VALID AND NORMALIZED
  req
    .check('email', 'Email must be between 3 to 40 characters')
    .matches(/.+\@.+\..+/)
    .withMessage('Wrong email format')
    .isLength({
      min: 4,
      max: 40
    });
  //Check for password
  req.check('password', 'Password is required').notEmpty();
  req
    .check('password')
    .isLength({ min: 6 })
    .withMessage('Password must contain at least 6 characters')
    .matches(/\d/)
    .withMessage('Password must contain a number');
  //Check for error
  const errors = req.validationErrors();
  //if error show the first one as they happend
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  //Proceed to next middleware
  next();
};

exports.userUpdateValidator = (req, res, next) => {
  //NAMES ARE NOT NULL
  req
    .check('names', 'Names are required')
    .notEmpty()
    .matches(/[a-zA-Z]{3,40}/);
  //LAST NAMES ARE NOT NULL
  req
    .check('last_names', 'Last names are required')
    .notEmpty()
    .matches(/[a-zA-Z]{3,40}/);
  //MOBILE MUST HAVE TEN DIGITS AND NOT BE NULL
  req
    .check('mobile', 'Mobile must have ten digits')
    .matches(/[0-9]{10}/)
    .notEmpty();
  //EMAIL VALID AND NORMALIZED
  req
    .check('email', 'Email must be between 3 to 40 characters')
    .matches(/.+\@.+\..+/)
    .withMessage('Wrong email format')
    .isLength({
      min: 4,
      max: 40
    });
  //COUNTRY IS NOT NULL
  req
    .check('country', 'Country is required')
    .notEmpty()
    .matches(/[a-zA-Z]{3,40}/);
  //STATE IS NOT NULL
  req
    .check('state', 'State is required')
    .notEmpty()
    .matches(/[a-zA-Z]{3,40}/);
  //CITY IS NOT NULL
  req
    .check('city', 'City is required')
    .notEmpty()
    .matches(/[a-zA-Z]{3,40}/);
  //ADDRESS IS NOT NULL
  req
    .check('address', 'Address is required')
    .notEmpty()
    .matches(/[a-zA-Z]{3,40}/);
  //Check for error
  const errors = req.validationErrors();
  //if error show the first one as they happend
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  next();
};

exports.forgotPasswordValidator = (req, res, next) => {
  req
    .check('email', 'Email must be between 3 to 40 characters')
    .notEmpty()
    .matches(/.+\@.+\..+/)
    .withMessage('Wrong email format')
    .isLength({
      min: 4,
      max: 40
    });
  //Check for error
  const errors = req.validationErrors();
  //if error show the first one as they happend
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  next();
};

exports.resetPasswordValidator = (req, res, next) => {
  //Check for password
  req.check('newPassword', 'Password is required').notEmpty();
  req
    .check('newPassword')
    .isLength({ min: 6 })
    .withMessage('Password must contain at least 6 characters')
    .matches(/\d/)
    .withMessage('Password must contain a number');

  //Check for error
  const errors = req.validationErrors();
  //if error show the first one as they happend
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  next();
};
