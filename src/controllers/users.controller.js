const dbQuery = require('../config');
const moment = require('moment-timezone');

const getUsers = async (req, res) => {
  try {
    const queryString =
      'SELECT id, identity_card, names, last_names, email, mobile, country, state, city, address FROM users';
    const response = await dbQuery.query(queryString);
    return res.status(200).json(response.rows);
  } catch (e) {
    return res.status(400).end();
  }
};

const getUserById = async (req, res) => {
  try {
    const queryString =
      'SELECT id, identity_card, names, last_names, gender, email, mobile, country, state, city, address, description  FROM users WHERE id = $1';
    const id = req.params.id;
    const response = await dbQuery.query(queryString, [id]);
    return res.status(200).json({ user: response.rows[0] });
  } catch (e) {
    res.status(400).end();
  }
};

const updateUser = async (req, res) => {
  try {
    const id = req.params.id;
    const {
      names,
      last_names,
      email,
      mobile,
      country,
      state,
      city,
      address,
      description
    } = req.body;
    updated_at = moment(new Date())
      .tz('America/Bogota')
      .format();
    const queryString =
      'UPDATE users SET  names = $2, last_names = $3, email = $4,  mobile = $5, country = $6, state = $7, city = $8, address = $9, description = $10, updated_at = $11 WHERE id = $1';

    const response = await dbQuery.query(queryString, [
      id,
      names,
      last_names,
      email,
      mobile,
      country,
      state,
      city,
      address,
      description,
      updated_at
    ]);

    return res.status(200).json({ message: 'User updated succesfuly' });
  } catch (e) {
    console.log(e);
    return res.status(400).end();
  }
};

const deleteUser = async (req, res) => {
  try {
    const id = req.params.id;
    const queryString = 'DELETE FROM users WHERE id = $1';
    const response = await dbQuery.query(queryString, [id]);

    return res.status(200).json({ message: 'User deleted succesfuly' });
  } catch (e) {
    console.log(e);
    return res.status(400).end();
  }
};

const hasAuthorization = (req, res, next) => {
  /*If there's an id in the params, if the auth exist and if the id 
 in the params matches the id of the auth*/

  let isSameUser =
    req.params.id && req.auth && req.params.id == req.auth.userId;
  const authorized = isSameUser;
  if (!authorized) {
    return res
      .status(403)
      .json({ error: 'User is not authorized to perform this action' });
  }
  next();
};

module.exports = {
  getUsers,
  getUserById,
  updateUser,
  deleteUser,
  hasAuthorization
};
