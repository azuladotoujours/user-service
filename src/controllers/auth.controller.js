const dbQuery = require('../config');
const Helper = require('../helpers/auth.helper');
const expressJwt = require('express-jwt');
const moment = require('moment-timezone');
const jwt = require('jsonwebtoken');
const { sendEmail } = require('../helpers/mail.helper');

exports.signUp = async (req, res) => {
  const {
    identity_card,
    names,
    last_names,
    gender,
    birthday,
    email,
    mobile,
    country,
    state,
    city,
    address,
    password,
    dni_type_id
  } = req.body;
  try {
    const userExists = await dbQuery.query(
      'SELECT * FROM users WHERE email = $1',
      [email]
    );

    if (userExists.rows[0]) {
      return res
        .status(403)
        .json({ error: 'User already signed up!' })
        .end();
    }
  } catch (e) {
    return res
      .status(404)
      .send(e)
      .end();
  }

  const hashed_password = Helper.hashPassword(password);
  console.log(hashed_password);
  const created_at = moment(new Date())
    .tz('America/Bogota')
    .format();

  const createUserQuery =
    'INSERT INTO users (identity_card, names,last_names, gender, birthday, email,  mobile, country,state,city,address,hashed_password,created_at, dni_type_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)';
  const values = [
    identity_card,
    names,
    last_names,
    gender,
    birthday,
    email,
    mobile,
    country,
    state,
    city,
    address,
    hashed_password,
    created_at,
    dni_type_id
  ];

  try {
    const response = await dbQuery.query(createUserQuery, values);

    res
      .status(200)
      .json({ message: 'User signed up succesfully', user: response.rows[0] });
  } catch (e) {
    console.log(e);
    res.status(500).send(e);
  }
};

exports.signIn = async (req, res) => {
  try {
    const queryString =
      'SELECT id, identity_card, names, last_names, email, balance, hashed_password FROM users WHERE email = $1';
    const response = await dbQuery.query(queryString, [req.body.email]);
    if (!response.rows[0]) {
      return res.status(400).json({ error: 'Email is not signed up!' });
    }

    if (
      !Helper.comparePassword(
        response.rows[0].hashed_password,
        req.body.password
      )
    ) {
      return res.status(400).json({ error: 'Incorrect password' });
    }

    const token = Helper.generateToken(response.rows[0].id);
    res.cookie('t', token, { expire: new Date() + 10 });
    const {
      id,
      names,
      last_names,
      identity_card,
      email,
      balance
    } = response.rows[0];
    return res.status(200).send({
      token,
      user: { id, names, last_names, identity_card, email, balance }
    });
  } catch (e) {
    console.log(e);
    return res.status(400).end();
  }
};

exports.forgotPassword = async (req, res) => {
  const { email } = req.body;
  let user;
  let token;

  try {
    let queryString = 'SELECT * FROM users WHERE email = $1 ';
    user = await dbQuery.query(queryString, [email]);
    token = jwt.sign({ id: user.rows[0].id }, process.env.JWT_SECRET);
    queryString =
      'INSERT INTO password_resets (email, token, created_at, users_id) VALUES ($1, $2, $3, $4)';
    const { id } = user.rows[0].id;
    const created_at = moment(new Date())
      .tz('America/Bogota')
      .format();
    const values = [email, token, created_at, id];
    dbQuery.query(queryString, values);
  } catch (e) {
    return res
      .status(401)
      .send({ message: 'User with that email does not exist' });
  }

  const emailData = {
    from: 'noreply@testing.com',
    to: email,
    subject: 'Password Reset Instructions',
    text: `Please use the following link to reset your password: ${process.env.CLIENT_URL}/api/v1/reset-password/${token}`
  };

  sendEmail(emailData);
  return res.status(200).json({ message: `Email has been sent to ${email}` });
};

exports.resetPassword = async (req, res) => {
  const resetPasswordToken = req.params.token;
  const { newPassword } = req.body;
  const correctToken = await verifyResetPasswordToken(resetPasswordToken);

  if (correctToken) {
    const decodedToken = jwt.decode(resetPasswordToken);
    const newHashedPassword = Helper.hashPassword(newPassword);
    queryString = 'UPDATE users SET hashed_password = $1 WHERE id = $2';
    try {
      await eraseResetPasswordToken(resetPasswordToken);
      await dbQuery.query(queryString, [newHashedPassword, decodedToken.id]);
      return res.status(200).json({ message: 'Password reseted successfuly' });
    } catch (e) {
      console.log(e);
    }
  } else {
    return res.status(404).json({ message: 'Token inválido' });
  }
};

const verifyResetPasswordToken = async token => {
  let queryString = 'SELECT users_id FROM password_resets WHERE token = $1';

  const response = await dbQuery.query(queryString, [token]);
  if (response.rows[0]) {
    return true;
  } else {
    return false;
  }
};

const eraseResetPasswordToken = async token => {
  let queryString = 'DELETE FROM password_resets WHERE token = $1';

  const response = await dbQuery.query(queryString, [token]);
  if (response.rows[0]) {
    return true;
  } else {
    return false;
  }
};

exports.requireSignIn = expressJwt({
  //if the token is valid, express jwt appends the verified users id
  //in an auth key to the request object
  secret: process.env.JWT_SECRET,
  userProperty: 'auth'
});
