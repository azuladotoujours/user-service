const nodeMailer = require('nodemailer');

exports.sendEmail = emailData => {
  const transporter = nodeMailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false,
    requireTLS: true,
    auth: {
      user: process.env.ETHEREAL_MAIL,
      pass: process.env.ETHEREAL_PASSWORD
    }
  });
  return transporter
    .sendMail(emailData)
    .then(info => console.log(`Message sent: ${info.response}`))
    .catch(err => console.log(`Problem sending email: ${err}`));
};
