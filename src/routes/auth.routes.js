const { Router } = require('express');
const router = Router();
const {
  userSignUpValidator,
  forgotPasswordValidator,
  resetPasswordValidator
} = require('../validator/validator');
const {
  signUp,
  signIn,
  forgotPassword,
  resetPassword
} = require('../controllers/auth.controller');

router.post('/signup', userSignUpValidator, signUp);
router.post('/signin', signIn);
router.post('/forgot-password', forgotPasswordValidator, forgotPassword);
router.post('/reset-password/:token', resetPasswordValidator, resetPassword);

module.exports = router;
