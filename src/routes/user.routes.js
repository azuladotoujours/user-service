const { Router } = require('express');
const router = Router();
const {
  getUsers,
  getUserById,
  updateUser,
  deleteUser,
  hasAuthorization
} = require('../controllers/users.controller');
const { userUpdateValidator } = require('../validator/validator');

const { requireSignIn } = require('../controllers/auth.controller');

router.get('/users', getUsers);
router.get('/user/:id', getUserById);
router.put(
  '/user/:id',
  requireSignIn,
  hasAuthorization,
  userUpdateValidator,
  updateUser
);
router.delete('/user/:id', requireSignIn, hasAuthorization, deleteUser);

module.exports = router;
