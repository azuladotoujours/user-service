CREATE DATABASE usersmicroservice;

CREATE TABLE dni_type (
    id VARCHAR(1) NOT NULL,
    description VARCHAR(20) NOT NULL,

        PRIMARY KEY (id)

);


CREATE TABLE position (
    id VARCHAR(1) NOT NULL,
    description VARCHAR(20) NOT NULL,
    name VARCHAR(30),

        PRIMARY KEY (id)

);

/*
Before the uuid, execute...

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

*/


CREATE TABLE users (
    id uuid DEFAULT uuid_generate_v4 (),
    identity_card VARCHAR(10) NOT NULL,
    names             VARCHAR(40) NOT NULL,
    last_names        VARCHAR(40) NOT NULL,
    gender            VARCHAR(1),
    birthday          DATE NOT NULL,
    email             VARCHAR(40) NOT NULL,
    mobile            VARCHAR(10) NOT NULL,
    country           VARCHAR(8) NOT NULL,
    state             VARCHAR(30) NOT NULL,
    city              VARCHAR(20) NOT NULL,
    address           VARCHAR(120) NOT NULL,
    description       VARCHAR(40),
    balance           INTEGER DEFAULT 0 NOT NULL,
    hashed_password   VARCHAR(100) NOT NULL,
    created_at        TIMESTAMPTZ NOT NULL,
    updated_at        TIMESTAMPTZ,
    dni_type_id       VARCHAR(1) REFERENCES dni_type(id),

    PRIMARY KEY (id)
);

CREATE TABLE password_resets (
    id uuid DEFAULT uuid_generate_v4 (),  
    email        VARCHAR(40) NOT NULL,
    token        VARCHAR(200) NOT NULL,
    created_at   TIMESTAMPTZ NOT NULL,
    users_id     uuid REFERENCES users(id),

    PRIMARY KEY(id)
);


